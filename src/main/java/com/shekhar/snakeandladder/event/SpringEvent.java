package com.shekhar.snakeandladder.event;

/**
 * @author Chandra Shekhar Yadav
 * @version 1.0
 * @Date 23-Aug-2017
 */
public class SpringEvent extends Event {

	@Override
	public SpringEvent execute(Integer nextState) {
		// don't move
		return this;
	}

	@Override
	public void acknowledge() {
		System.out
				.println(String.format("Can't Move. Spring bounces you back on the same position. You position: %s", player.getPostion()));
	}

}
