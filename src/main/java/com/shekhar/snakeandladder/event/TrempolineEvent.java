package com.shekhar.snakeandladder.event;

/**
 * @author Chandra Shekhar Yadav
 * @version 1.0
 * @Date 23-Aug-2017
 */
public class TrempolineEvent extends Event {

	@Override
	public TrempolineEvent execute(Integer nextState) {
		player.setPostion(player.getPostion() + (2 * player.getDiceFace()));
		return this;
	}

	@Override
	public void acknowledge() {
		System.out.println(String.format("You got trempoline. You postion is %s", player.getPostion()));
	}

}
